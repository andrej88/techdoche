# Techdoche

Techdoche is a framework for creating technical documentation websites. Its primary use-case is for writing documentation of digital tools such as programs, programming languages, libraries, or APIs.

# Overall goals
- Embrace semantic HTML
- Automatically generate the table of contents
- Use a familiar markup syntax based on Markdown (due to its existing popularity) with certain extensions:
    - Intuitive cross-reference syntax
    - Embeddable images (needs alt-text)
    - Tables
    - Description lists
    - Multiline code listings
- Allow selecting different versions of the documentation
- Techdoche's documentation will be written in, of course, Techdoche itself 🙂


## Code Listings

- Automatic syntax highlighting
- Allow user-defined syntax highlighting (somehow...), useful if the documentation is for a new programming language, for example.


## Versioning
- Provide a way to select different versions of the documentation.
- Allow specifying a certain version as the default (e.g. `latest` or `stable`).
- Do not restrict the version format to Semver or anything.
- Allow the same version of the software to have multiple revisions of its documentation.

Perhaps use git to find and organize the available versions the user can choose. Supposing this is the documentation's git repository:

```
O  <--  branch: techdoche/release-c
|
O
|  O  <--  branch: techdoche/release-b
O  |
|  O
O  |  O  <--  tag: new-styling-test
|  O /
O  |/
|  O
O /
|/
O
|
O
|
O  <--  tag: techdoche/release-a
|
O
|
O
```

This would result in the following versions being selectable:

- release-c
- release-b
- release-a

The author can configure display names for each version something like this:

```toml
release-c = "Catatonic Cassowary"
release-b = "Bored Bee"
release-a = "Apathetic Alligator"
```

But using the repo's own git history may be a bit weird when the config is located in the same repo... maybe the config file is always the currently checked out version, while document contents may be fetched from git. But then it must be absolutely clear what is fetched from the git history.

It's also a bit weird that the documentation source repo changes its own working tree while compiling, or re-clones itself, or whatever. Maybe this git approach isn't the best...


## Future Work
- Offer both a JS-free version (via `<noscript>`?) and a faster, JS-supported version (by generating components for e.g. Svelte?)
    - Some of these goals may be difficult to achieve without scripting
    - Can the JS and non-JS variants use the same URLs?
- Executable + editable code listings
- Mobile-friendly interface
- Nice formatting when printed (using media queries?)
- Allow user to choose the styling: 
    - light and dark modes. This means allowing authors to provide "light" and "dark" image files too.
    - Minimal stylesheet with fonts, colors, etc. defaulting to the browser's style
    - optional code ligatures
    - Could go the extra mile and offer dyslexia-friendly fonts, adjustable color contrast, etc. but possibly most use cases would be covered by offering a minimally-styled variant so that the browser's styles may be used.


# An Intermediate Representation for API Documentation?
![A diagram showing how various embedded documentation formats could be compiled into an agnostic intermediate representation and then output into any other format.](doc_ir_diagram.svg)


## The Problem

Programming languages each have their own syntax for embedded documentation. That documentation ends up in a number of places (a website, an IDE, a PDF). This results in a lot of work for everyone.

## A Solution?

Introduce a kind of "Intermediate Representation" for documentation. This way, language creators only need to worry about translating their documentation into the IR, while website authors and IDE developers only need to know how to parse the IR and display it.
